package rentacar.service;

import java.util.List;

import rentacar.model.Company;

public interface CompanyService {

	public List<Company> findAll();
	public Company findOne(Long id);
	public Company save(Company c);
}
