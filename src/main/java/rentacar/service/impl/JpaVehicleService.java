package rentacar.service.impl;

import javax.persistence.EntityNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import rentacar.model.Vehicle;
import rentacar.model.Rent;
import rentacar.repository.VehicleRepository;
import rentacar.repository.RentRepository;
import rentacar.service.VehicleService;

@Service
public class JpaVehicleService 
	implements VehicleService {

	@Autowired
	private VehicleRepository vehicleRepository;
	@Autowired
	private RentRepository rentRepository;
	
	@Override
	public Page<Vehicle> findAll(int page) {
		return vehicleRepository.findAll(PageRequest.of(page, 5));
	}

	@Override
	public Vehicle findOne(Long id) {
		return vehicleRepository.findById(id)
								.orElseThrow(() -> new EntityNotFoundException());
	}

	@Override
	public Vehicle save(Vehicle vehicle) {
		return vehicleRepository.save(vehicle);
	}


	@Override
	public Vehicle remove(Long id) {
		Vehicle toDelete = vehicleRepository.findById(id)
											.orElseThrow(() -> new EntityNotFoundException());;
		if(toDelete != null) {
			vehicleRepository.deleteById(id);
		}
		
		return toDelete;
	}

	@Override
	public Page<Vehicle> search(String model, Integer minYear, Integer maxConsumption, int page) {
		
		if(model != null) {
			model = '%' + model + '%';
		}
		return vehicleRepository.search(model, minYear, maxConsumption, PageRequest.of(page, 5));
	}
	
	@Override
	public Rent rent(Long vehicleId) {
		
		if(vehicleId == null) {
			throw new IllegalArgumentException("Id cannot be null!");
		}
		
		Vehicle vehicle = vehicleRepository.findById(vehicleId)
										   .orElseThrow(() -> new EntityNotFoundException("Couldn't find vehicle with id " + vehicleId));
		

		if(!vehicle.isRent()) {
			Rent rent = new Rent();
			rent.setVehicle(vehicle);

			vehicle.setRent(true);

			rentRepository.save(rent);
			vehicleRepository.save(vehicle);

			return rent;
		}
		
		return null;
	}

	@Override
	public Page<Vehicle> findByCompanyId(int page, Long id) {
		return vehicleRepository.findByCompanyId(id, PageRequest.of(page, 5));
	}


}
