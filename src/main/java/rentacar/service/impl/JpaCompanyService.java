package rentacar.service.impl;

import java.util.List;

import javax.persistence.EntityNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import rentacar.model.Company;
import rentacar.repository.CompanyRepository;
import rentacar.service.CompanyService;

@Service
public class JpaCompanyService 
	implements CompanyService {

	@Autowired
	private CompanyRepository companyRepository;
	
	@Override
	public List<Company> findAll() {
		return companyRepository.findAll();
	}

	@Override
	public Company findOne(Long id) {
		return companyRepository.findById(id)
								 .orElseThrow(() -> new EntityNotFoundException());
	}

	@Override
	public Company save(Company i) {
		return companyRepository.save(i);
	}

}
