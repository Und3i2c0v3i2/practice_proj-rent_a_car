package rentacar.service;

import org.springframework.data.domain.Page;
import org.springframework.data.repository.query.Param;

import rentacar.model.Vehicle;
import rentacar.model.Rent;

public interface VehicleService {

	public Page<Vehicle> findAll(int page);
	public Vehicle findOne(Long id);
	public Vehicle save(Vehicle v);
	public Vehicle remove(Long id);
	public Page<Vehicle> search(
			@Param("model") String naziv, 
			@Param("minYear") Integer minYear, 
			@Param("maxConsumption") Integer maxConsumption, 
			int page);
	public Rent rent(Long vehicleId);
	public Page<Vehicle> findByCompanyId(int page, Long id);
	
	
}
