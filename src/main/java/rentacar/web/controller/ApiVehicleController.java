package rentacar.web.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import rentacar.model.Vehicle;
import rentacar.model.Rent;
import rentacar.service.VehicleService;
import rentacar.support.VehicleDTOToVehicle;
import rentacar.support.VehicleToVehicleDTO;
import rentacar.support.RentToRentDTO;
import rentacar.web.dto.VehicleDTO;
import rentacar.web.dto.RentDTO;

@Controller
@RequestMapping(value="api/vehicles")
public class ApiVehicleController {
	
	@Autowired
	private VehicleService vehicleService;
	@Autowired
	private VehicleDTOToVehicle toVehicle;
	@Autowired
	private VehicleToVehicleDTO toDto;
	@Autowired
	private RentToRentDTO rentDto; 
	
	@GetMapping
	public ResponseEntity<List<VehicleDTO>> getAll(
			@RequestParam(required=false) String model,
			@RequestParam(required=false) Integer minYear,
			@RequestParam(required=false) Integer maxConsumption,
			@RequestParam(value="page", defaultValue="0") int page){
		
		Page<Vehicle> vehiclePage = null;
		
		if(model != null || minYear != null || maxConsumption != null) {
			vehiclePage = vehicleService.search(model, minYear, maxConsumption, page);
		}
		else {
			vehiclePage = vehicleService.findAll(page);
		}

		HttpHeaders headers = new HttpHeaders();
		headers.add("totalPages", Integer.toString(vehiclePage.getTotalPages()) );
		
		return new ResponseEntity<>(
				toDto.convert(vehiclePage.getContent()), headers, HttpStatus.OK);
	}
	
	
	@GetMapping("/{id}")
	ResponseEntity<VehicleDTO> getOne(@PathVariable Long id){
		
		Vehicle vehicle = vehicleService.findOne(id);
				
		return new ResponseEntity<>(toDto.convert(vehicle), HttpStatus.OK);
	}
	
	@DeleteMapping(value="/{id}")
	ResponseEntity<VehicleDTO> delete(@PathVariable Long id){
		
		Vehicle deleted = vehicleService.remove(id);
		
		return new ResponseEntity<>(toDto.convert(deleted),HttpStatus.OK);
	}
	
	
	@PostMapping
	public ResponseEntity<VehicleDTO> add(@Validated 
										  @RequestBody VehicleDTO newVehicleDTO){
		
		Vehicle saved = vehicleService.save(toVehicle.convert(newVehicleDTO));
		
		return new ResponseEntity<>(toDto.convert(saved), HttpStatus.CREATED);
	}
	
	
	@PutMapping("/{id}")
	public ResponseEntity<VehicleDTO> edit(@Validated 
										   @RequestBody VehicleDTO vehicleDTO,
										   @PathVariable Long id){
		
		if(!id.equals(vehicleDTO.getId())){
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		
		Vehicle persisted = vehicleService.save(toVehicle.convert(vehicleDTO));
		
		return new ResponseEntity<>(toDto.convert(persisted), HttpStatus.OK);
	}
	
	@PostMapping(value="/{id}/rent")
	public ResponseEntity<RentDTO> rent(@PathVariable Long id){
		
		Rent r = vehicleService.rent(id);
		
		return new ResponseEntity<>(rentDto.convert(r), HttpStatus.OK);

		
	}
	
//	@ExceptionHandler(value=DataIntegrityViolationException.class)
//	public ResponseEntity<Void> handle() {
//		return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
//	}

}
