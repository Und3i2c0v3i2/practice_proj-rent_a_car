package rentacar.web.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import rentacar.model.Vehicle;
import rentacar.model.Company;
import rentacar.service.VehicleService;
import rentacar.service.CompanyService;
import rentacar.support.VehicleToVehicleDTO;
import rentacar.support.CompanyToCompanyDTO;
import rentacar.web.dto.VehicleDTO;
import rentacar.web.dto.CompanyDTO;

@Controller
@RequestMapping(value="api/companies")
public class ApiCompanyController {

	@Autowired
	private CompanyService companyService;
	@Autowired
	private CompanyToCompanyDTO toDto;
	@Autowired
	private VehicleService vehicleService;
	@Autowired
	private VehicleToVehicleDTO automobilDto;
	
	@RequestMapping(method=RequestMethod.GET)
	public ResponseEntity<List<CompanyDTO>> getAll() {
		
		List<Company> companies = companyService.findAll();
		
		return ResponseEntity.ok()
							 .body(toDto.convert(companies));
	}
	
	@RequestMapping(method=RequestMethod.GET, value="/{id}")
	public ResponseEntity<CompanyDTO> getOne(@PathVariable Long id) {
		
		Company c = companyService.findOne(id);
		
		return new ResponseEntity<>(toDto.convert(c), HttpStatus.OK);
	}
	
	
	@RequestMapping(method=RequestMethod.GET, value="/{id}/vehicles")
	public ResponseEntity<List<VehicleDTO>> getAutomobiliKompanije(
			@PathVariable Long id,
			@RequestParam(defaultValue="0") int page) {
		
		Page<Vehicle> automobili = vehicleService.findByCompanyId(page, id);

		HttpHeaders headers = new HttpHeaders();
		headers.add("totalPages", Integer.toString(automobili.getTotalPages()) );
		
		return new ResponseEntity<>(automobilDto.convert(automobili.getContent()), headers,	HttpStatus.OK);
	}
}
