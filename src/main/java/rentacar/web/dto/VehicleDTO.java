package rentacar.web.dto;


public class VehicleDTO {
	
	private Long id;
	private String model;
	private String registration;
	private int year;
	private int fuelConsumption;
	private boolean isRent;
	private Long companyId;
	private String companyName;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	public String getRegistration() {
		return registration;
	}
	public void setRegistration(String registration) {
		this.registration = registration;
	}
	public Long getCompanyId() {
		return companyId;
	}
	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		this.year = year;
	}
	public int getFuelConsumption() {
		return fuelConsumption;
	}
	public void setFuelConsumption(int consumption) {
		this.fuelConsumption = consumption;
	}
	public boolean isRent() {
		return isRent;
	}
	public void setIsRent(boolean rent) {
		this.isRent = rent;
	}
	
}
