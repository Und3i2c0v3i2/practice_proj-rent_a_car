package rentacar.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import rentacar.model.Vehicle;
import rentacar.web.dto.VehicleDTO;

@Component
public class VehicleToVehicleDTO 
	implements Converter<Vehicle, VehicleDTO>{

	@Override
	public VehicleDTO convert(Vehicle a) {
		VehicleDTO retVal = new VehicleDTO();
		retVal.setId(a.getId());
		retVal.setModel(a.getModel());
		retVal.setRegistration(a.getRegistration());
		retVal.setYear(a.getYear());
		retVal.setFuelConsumption(a.getFuelConsumption());
		retVal.setIsRent(a.isRent());
		retVal.setCompanyId(a.getCompany().getId());
		retVal.setCompanyName(a.getCompany().getName());
		
		return retVal;
	}

	public List<VehicleDTO> convert(List<Vehicle> vehicles) {
		List<VehicleDTO> retVal = new ArrayList<>();
		for(Vehicle v : vehicles) {
			retVal.add(convert(v));
		}
		return retVal;
	}
}
