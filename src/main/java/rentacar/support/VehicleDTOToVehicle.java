package rentacar.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import rentacar.model.Vehicle;
import rentacar.model.Company;
import rentacar.service.VehicleService;
import rentacar.service.CompanyService;
import rentacar.web.dto.VehicleDTO;


@Component
public class VehicleDTOToVehicle 
	implements Converter<VehicleDTO, Vehicle> {

	@Autowired
	private VehicleService vehicleService;
	@Autowired 
	private CompanyService companyService;
	
	@Override
	public Vehicle convert(VehicleDTO dto) {
		Vehicle vehicle;
		
		if(dto.getId() == null) {
			vehicle = new Vehicle();
		} else {
			vehicle = vehicleService.findOne(dto.getId());
		}
		
		Company k = companyService.findOne(dto.getCompanyId());
								 
		if(k != null) {
			vehicle.setCompany(k);
		}
		
		vehicle.setModel(dto.getModel());
		vehicle.setRegistration(dto.getRegistration());
		vehicle.setYear(dto.getYear());
		vehicle.setFuelConsumption(dto.getFuelConsumption());
			
		return vehicle;
	}
	
	public List<Vehicle> convert(List<VehicleDTO> dtos) {
		List<Vehicle> retVal = new ArrayList<>();
		for(VehicleDTO dto : dtos) {
			retVal.add(convert(dto));
		}
		return retVal;
	}

}
