package rentacar.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import rentacar.model.Company;
import rentacar.web.dto.CompanyDTO;

@Component
public class CompanyToCompanyDTO 
	implements Converter<Company, CompanyDTO>{

	@Override
	public CompanyDTO convert(Company company) {
		CompanyDTO retVal = new CompanyDTO();
		retVal.setId(company.getId());
		retVal.setName(company.getName());
		
		return retVal;
	}

	public List<CompanyDTO> convert(List<Company> companies) {
		List<CompanyDTO> retVal = new ArrayList<>();
		for(Company c : companies) {
			retVal.add(convert(c));
		}
		return retVal;	
	}
}
