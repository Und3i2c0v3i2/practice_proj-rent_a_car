package rentacar.support;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import rentacar.model.Rent;
import rentacar.web.dto.RentDTO;

@Component
public class RentToRentDTO 
	implements Converter<Rent, RentDTO>{

	@Override
	public RentDTO convert(Rent source) {

		RentDTO dto = new RentDTO();
		dto.setId(source.getId());
		return dto;
	}

}
