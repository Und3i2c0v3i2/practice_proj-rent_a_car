package rentacar;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RentACarApp {

	public static void main(String[] args) {
		SpringApplication.run(RentACarApp.class, args);

	}
}
