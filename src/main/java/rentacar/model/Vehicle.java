package rentacar.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotEmpty;


@Entity
public class Vehicle {

	@Id
	@GeneratedValue
	@Column
	private Long id;
	@Column(nullable=false)
	@NotEmpty
	private String model;
	@Column(nullable=false, unique=true)
	private String registration;
	@Column(nullable=false)
	@Max(value=9999)
	private int year;
	@Max(value=99)
	private int fuelConsumption;
	private boolean isRent;
	@ManyToOne
	private Company company;
	@OneToMany(mappedBy="vehicle",fetch=FetchType.LAZY,cascade=CascadeType.ALL)
	private List<Rent> rented = new ArrayList<>();
	
		
	public Vehicle() {
		super();
		this.isRent = false;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public String getRegistration() {
		return registration;
	}

	public void setRegistration(String registration) {
		this.registration = registration;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public int getFuelConsumption() {
		return fuelConsumption;
	}

	public void setFuelConsumption(int fuelConsumption) {
		this.fuelConsumption = fuelConsumption;
	}

	public boolean isRent() {
		return isRent;
	}

	public void setRent(boolean isRent) {
		this.isRent = isRent;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
		if(company!=null && !company.getVehicles().contains(this)){
			company.getVehicles().add(this);
		}
	}
	public List<Rent> getRented() {
		return rented;
	}
	public void setRented(List<Rent> rented) {
		this.rented = rented;
	}
	public void addRent(Rent rent){
		this.rented.add(rent);

		if(!this.equals(rent.getVehicle())){
			rent.setVehicle(this);
		}
	}
}
