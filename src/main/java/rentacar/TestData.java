package rentacar;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import rentacar.model.Vehicle;
import rentacar.model.Company;
import rentacar.service.VehicleService;
import rentacar.service.CompanyService;

@Component
public class TestData {

	@Autowired
	private CompanyService companyService;
	@Autowired
	private VehicleService vehicleService;
	
	
	@PostConstruct
	public void init() {
		
		// Dummy Data
		
		Company k1 = new Company();
		k1.setName("SuRent");
		Company k2 = new Company();
		k2.setName("NsRent");
		Company k3 = new Company();
		k3.setName("BgRent");
		
		companyService.save(k1);
		companyService.save(k2);
		companyService.save(k3);
		
		Vehicle a1 = new Vehicle();
		a1.setModel("Nissan Prairie");
		a1.setFuelConsumption(10);
		a1.setYear(2014);
		a1.setCompany(k1);
		a1.setRegistration("ZR-1840-MO");
		
		Vehicle a2 = new Vehicle();
		a2.setModel("Nissan Micra");
		a2.setFuelConsumption(10);
		a2.setYear(2018);
		a2.setCompany(k1);
		a2.setRegistration("NS-4841-XV");
		
		Vehicle a3 = new Vehicle();
		a3.setModel("Fiat Punto");
		a3.setFuelConsumption(6);
		a3.setYear(2007);
		a3.setCompany(k2);
		a3.setRegistration("NS-4978-NB");
		
		Vehicle a4 = new Vehicle();
		a4.setModel("Audi A4");
		a4.setFuelConsumption(6);
		a4.setYear(2011);
		a4.setCompany(k3);
		a4.setRegistration("SU-1203-HF");
		
		Vehicle a5 = new Vehicle();
		a5.setModel("Alfa Romeo Giulietta");
		a5.setFuelConsumption(8);
		a5.setYear(2015);
		a5.setCompany(k2);
		a5.setRegistration("NS-1841-ZI");
		
		Vehicle a6 = new Vehicle();
		a6.setModel("BMW 320 D");
		a6.setFuelConsumption(12);
		a6.setYear(2000);
		a6.setCompany(k3);
		a6.setRegistration("BG-9783-RF");
		
		Vehicle a7 = new Vehicle();
		a7.setModel("BMW 520");
		a7.setFuelConsumption(12);
		a7.setYear(2015);
		a7.setCompany(k1);
		a7.setRegistration("BG-3164-BB");
		
		Vehicle a8 = new Vehicle();
		a8.setModel("BMW 320 D");
		a8.setFuelConsumption(12);
		a8.setYear(2018);
		a8.setCompany(k2);
		a8.setRegistration("BG-1619-PL");
		
		Vehicle a9 = new Vehicle();
		a9.setModel("Dacia Duster");
		a9.setFuelConsumption(8);
		a9.setYear(2010);
		a9.setCompany(k2);
		a9.setRegistration("BG-3162-YT");
		
		Vehicle a10 = new Vehicle();
		a10.setModel("Tesla Roadster");
		a10.setFuelConsumption(0);
		a10.setYear(2018);
		a10.setCompany(k3);
		a10.setRegistration("NS-1584-HG");
		
		vehicleService.save(a1);
		vehicleService.save(a2);
		vehicleService.save(a3);
		vehicleService.save(a4);
		vehicleService.save(a5);
		vehicleService.save(a6);
		vehicleService.save(a7);
		vehicleService.save(a8);
		vehicleService.save(a9);
		vehicleService.save(a10);
	}
}
