package rentacar.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import rentacar.model.Rent;

@Repository
public interface RentRepository 
	extends JpaRepository<Rent, Long>{

}
