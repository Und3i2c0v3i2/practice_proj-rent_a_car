package rentacar.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import rentacar.model.Vehicle;

@Repository
public interface VehicleRepository 
	extends JpaRepository<Vehicle, Long>{

	@Query("SELECT v FROM Vehicle v WHERE "
			+ "(:model IS NULL or v.model like :model ) AND "
			+ "(:minYear IS NULL OR v.year >= :minYear) AND "
			+ "(:maxConsumption IS NULL or v.fuelConsumption <= :maxConsumption ) "
			)
	public Page<Vehicle> search(
			@Param("model") String model, 
			@Param("minYear") Integer minYear, 
			@Param("maxConsumption") Integer maxConsumption,
			Pageable pageRequest);

	public Page<Vehicle> findByCompanyId(Long id, Pageable pageRequest);

}
