var mainApp = angular.module("mainApp", ["ngRoute"]);

mainApp.config(['$routeProvider', function($routeProvider) {

	$routeProvider
		.when('/', {
			templateUrl : '/app/html/home.html',
			controller: 'homeController'
		})
		.when('/vehicles/edit/:vid', {
			templateUrl : '/app/html/edit.html'
		})
		.otherwise({
			redirectTo: '/'
		});
}]);

mainApp.controller("homeController", function($scope, $location, $http, $routeParams){
	
	$scope.message = "Rent - A - Car";

	var baseCompaniesUrl = "/api/companies";
	var baseVehiclesUrl = "/api/vehicles";


	$scope.page = 0;
	$scope.totalPages = 0;

	$scope.companies = [];
	$scope.vehicles =[];
	
	$scope.saved = {};

	$scope.searchParams = {};
	$scope.searchParams.model = "";
	$scope.searchParams.minYear = "";
	$scope.searchParams.maxConsumption = "";



//	---------------------------------------------------------------------------------
// 			
//	---------------------------------------------------------------------------------

	var getCompanies = function(){
		$http.get(baseCompaniesUrl).then(
			function(response){
        		$scope.companies = response.data;
			},
			function(){
        		alert("Couldn't get companies");
			}
    	);
  	}

	getCompanies();


//   ------------------------------------------------------------------------------------
//     
//   ------------------------------------------------------------------------------------

	var getVehicles = function(){
		var config = {params: {}};
		config.params.page = $scope.page;

		// provera da li postoje config.params parametri za pretragu
		if($scope.searchParams.model != ""){
			config.params.model = $scope.searchParams.model;
		}
		if($scope.searchParams.minYear != ""){
			config.params.minYear = $scope.searchParams.minYear;
		}
		if($scope.searchParams.maxConsumption != ""){
			config.params.maxConsumption = $scope.searchParams.maxConsumption;
		}
				
		config.params.page = $scope.page;

		$http.get(baseVehiclesUrl, config).then(
			function(response){
				$scope.vehicles = response.data;
				$scope.totalPages = response.headers("totalPages");
			},
			function(){
				alert("Couldn't get vehicles");
			}
		);
	}

	getVehicles();
				
	$scope.getVehicle = function(id){
		var getVehicleUrl = baseVehiclesUrl + "/" + id;
		$http.get(getVehicleUrl).then(
			function(response){
				$scope.vehicle = response.data;
			},
			function(){
				alert("Couldn't get vehicle");
			}
		);
	}

	$scope.remove = function(id){
		var deleteVehicleUrl = baseVehiclesUrl + "/" + id;
		$http.delete(deleteVehicleUrl).then(
			function(){
				getVehicles();
			},
			function(){
				alert("Couldn't delete vehicle");
			}
		);
	}

	$scope.save = function(){
		$http.post(baseVehiclesUrl, $scope.saved).then(
			function success(response){
				getVehicles();
				$scope.saved = {};
				$scope.add.$setPristine();
				$scope.add.$setUntouched();
			},
			function error(){
				alert("Couldn't save vehicle");
			}
		);
	}

	
	$scope.goToEdit = function(id){
		$location.path("/vehicles/edit/" + id);
	}


	$scope.rent = function(id) {
		$http.post(baseVehiclesUrl + "/" + id + "/rent").then(
			function(response){
				alert("Enjoy your ride!");
				getVehicles();
			},
			function(){
				alert("Something went wrong :(");
			}
		);			
	}

// ----------------------------------------------------
// 			PRETRAGA I PAGINACIJA 
// ----------------------------------------------------

	$scope.doSearch = function(){
		$scope.page = 0;
		getVehicles();
	}

	$scope.changePage = function(direction){
		$scope.page = $scope.page + direction;
		getVehicles();
	}

});
