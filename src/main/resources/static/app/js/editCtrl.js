mainApp.controller("editController", function($scope, $location, $http, $routeParams){

	var urlEdit = "/api/vehicles/" + $routeParams.vid;
	$scope.companies = [];
	$scope.edited = {};

	var getVehicleToEdit = function(){
		$http.get(urlEdit).then(
			function success(response){
				$scope.edited = response.data;
			},
			function error(){
				alert("Couldn't fetch vehicle for editing");
			}
		);
	}

	var getCompanies = function(){
		$http.get("/api/companies").then(
			function(response){
				$scope.companies = response.data;
				getVehicleToEdit();
			},
			function(){
				alert("Couldn't fetch companies for editing");
			}
		);
	}
	
	getCompanies();
	

	$scope.edit = function(){
		$http.put(urlEdit, $scope.edited).then(
			function success(){
				$location.path("/");
			},
			function error(){
				alert("Couldn't edit vehicle");
			}
		);
	}

});
